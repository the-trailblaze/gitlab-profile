## Stateless DAO

The project moved here - https://gitlab.com/stateless-dao/

## License

All projects in this group are licensed under the AGPL-3.0-only license if other open-source license isn't set directly for some reasons, reasons should be explained. 
Decision about supporting later versions of the AGPL license will be done after they are released, and after review. 

## ADR
The AGPL-3.0 license is chosen to ensure that the code of the projects will be open to the users of the projects in this group.

## Other licenses info
Current list of other supported open-source licenses:
- Apache 2.0

## Notes
The copy of the licence text can be found in the LICENSE,
the source of the text - https://www.gnu.org/licenses/agpl-3.0-standalone.html